﻿$(document).ready(function () {

});

$('#btn_GetData').bind('click', function () {
    //Ajax call for student data
    $.ajax({
        url: '/Home/GetAllStudentData/',
        type: 'GET',
        success: function (data, xhr, settings) {
            $('#div_Students').remove();
            $('#div_StudentData').html(data);
            $('#btn_Close').removeClass('hidden');
        },
        error: function (xhr, textstatus, error) {
            
        }
    });
});


$('#btn_Close').bind('click', function () {
   
    $('#div_Students').remove();
    $('#btn_Close').addClass('hidden');
});