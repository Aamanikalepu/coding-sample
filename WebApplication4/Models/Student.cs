﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
   public enum StudentType { UNDERGRADUATE,GRADUATE,DIPLOMA}
    //Modal class for the type Student and its properties
   public abstract class Student
    {
        int id;
        string name;
        string phonenumber;
        Address addressObj;
        StudentType type;
        Transcript stuTranscript;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Phonenumber
        {
            get
            {
                return phonenumber;
            }
            set
            {
                phonenumber = value;
            }
        }
        public Address Addressobj
        {
            get
            {
                return addressObj;
            }
            set
            {
                addressObj = value;
            }
        }

        public StudentType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public Transcript StuTranscript
        {
            get
            {
                return stuTranscript;
            }
            set
            {
                stuTranscript = value;
            }
        }


        public virtual bool  RegisterForClasses() { return true; }
        public virtual string  BuyCafeteriaCredits() { return "success"; }
        public virtual Transcript  RetrieveStudentTranscript() { return StuTranscript; }

    }

    public class Address
    {
        string line1;
        string line2;
        string state;
        string zip;
        public string Line1 {
            get
            {
                return line1;
            }
            set {
                line1 = value;
            }
        }
        public string Line2
        {
            get
            {
                return line2;
            }
            set
            {
                line2 = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public string Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }
    }
    public class Transcript
    {
     
    }


}
