﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Models
{
    public class SchoolSystem
    {
        public List<Student> StudentList { get; set; }

        public SchoolSystem()
        {
            StudentList = new List<Student>();
        }
    }
}