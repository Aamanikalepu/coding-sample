﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.BusinessLogic;

namespace WebApplication4.Controllers
{
    public class BaseController : Controller
    {
        //Schoolsystem service layer that contains the main business logic
        protected SchoolServiceLayer serviceLayer { get; set; }     

        public BaseController()
        {
            serviceLayer = new SchoolServiceLayer();
        }
    }
}