﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    //Main controller to perform the actions
    //Contacts the school system service layer to perform the actions
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        //Gets all the students information in the school and 
        //returns back to view to format and display information
        public ActionResult GetAllStudentData()
        {
            SchoolSystem school = serviceLayer.GetSchoolSystem();

            return PartialView("_Student", school);
        }

        //Main action for retrieving transcript for the given student id 
        public Transcript RetrieveTranscriptForUser(int loggedStudentId)
        {     
            return serviceLayer.RetrieveTranscript(loggedStudentId);                   
        }
        //Main action for buying cafeteria credits for the given student id 
        public string BuyCafeteriaCreditsForUser(int loggedStudentId)
        {
            return serviceLayer.BuyCafeteriaCredits(loggedStudentId);
        }
        //Main action for reistering for classes for the given student id 
        public bool RegisterStudentForClassesForUser(int loggedStudentId)
        {
            return serviceLayer.RegisterStudentForClasses(loggedStudentId);
        }
    }
}