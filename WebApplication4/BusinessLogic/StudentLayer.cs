﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;

namespace WebApplication4.BusinessLogic
{
    //categories of students and thier respective behaviour implementations that are overriden
    public class GraduateStudent : Student
    {

        public GraduateStudent() : base()
        {
            Addressobj = new Address();
            Type = StudentType.GRADUATE;
        }
        public override bool RegisterForClasses() { Console.WriteLine(" Graduate register for classes");return true; }
        public override string BuyCafeteriaCredits() { Console.WriteLine(" Graduate Cafeteria Credits"); return "success"; }
        public override Transcript RetrieveStudentTranscript() { Console.WriteLine(" Graduate Retrieve Transcript"); return StuTranscript; }

    }
    public class UnderGraduateStudent : Student
    {

        public UnderGraduateStudent() : base()
        {
            Addressobj = new Address();
            Type = StudentType.UNDERGRADUATE;
        }
        public override bool RegisterForClasses() { Console.WriteLine(" UnderGraduate register for classes"); return true; }
        public override string BuyCafeteriaCredits() { Console.WriteLine(" UnderGraduate Cafeteria Credits"); return "success"; }
        public override Transcript RetrieveStudentTranscript() { Console.WriteLine(" UnderGraduate Retrieve Transcript"); return StuTranscript; }

    }
    public class DiplomaStudent : Student
    {

        public DiplomaStudent() : base()
        {
            Addressobj = new Address();
            Type = StudentType.DIPLOMA;
        }
        public override bool RegisterForClasses() { Console.WriteLine(" Diploma register for classes"); return true; }
        public override string BuyCafeteriaCredits() { Console.WriteLine(" Diploma Cafeteria Credits"); return "success"; }
        public override Transcript RetrieveStudentTranscript() { Console.WriteLine(" Diploma Retrieve Transcript"); return StuTranscript; }

    }
}