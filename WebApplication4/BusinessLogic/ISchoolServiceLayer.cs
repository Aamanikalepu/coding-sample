﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.BusinessLogic
{
    public interface ISchoolServiceLayer
    {
        SchoolSystem GetSchoolSystem();

        bool RegisterStudentForClasses(int currentStudent);

        string BuyCafeteriaCredits(int currentStudent);

        Transcript RetrieveTranscript(int currentStudent);
       
    }
}
