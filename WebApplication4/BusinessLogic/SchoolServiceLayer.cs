﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.Repository;

namespace WebApplication4.BusinessLogic
{
    public class SchoolServiceLayer : ISchoolServiceLayer
    {
        //Contacts the repository to retrieve the model entities
        
        Repository<SchoolSystem> repository = new Repository<SchoolSystem>();

        //Return the complete school system that has the list of students
        public SchoolSystem GetSchoolSystem()
        {     
            return repository.GetAll();
        }

        // Register classes for the student depending on the type using overriding
        public bool RegisterStudentForClasses(int currentStudentID)
        {

            Student currentStudent = GetStudentById(currentStudentID);
            //Exhibits behavior depending upon the specific student type
            //returns success or failure 
           return currentStudent.RegisterForClasses();
        }

        public string BuyCafeteriaCredits(int currentStudentID)
        {
            Student currentStudent = GetStudentById(currentStudentID);
            //Exhibits behavior depending upon the specific student type
            //return error or success message
            return currentStudent.BuyCafeteriaCredits();
        }
        public Transcript RetrieveTranscript(int currentStudentID)
        {
           Student currentStudent =  GetStudentById(currentStudentID);
            //Exhibits behavior depending upon the specific student type
            //retrieves and return transcript
            return currentStudent.RetrieveStudentTranscript();
        }

        public Student GetStudentById(int currentStudentID)
        {
            Repository<Student> studentRepository = new Repository<Student>();
            Student currentStudent = studentRepository.GetById(currentStudentID);
            return currentStudent;
            
        }

    }

}