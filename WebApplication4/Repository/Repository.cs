﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.BusinessLogic;

namespace WebApplication4.Repository
{
    //Generic repository
    public class Repository<T> : IRepository<T>
    {
        public T GetById(int Id)
        {
            try
            {
                //Mock Call
                return (T)Convert.ChangeType(CreateSchoolSystem().StudentList.FirstOrDefault(x => x.Id == Id), typeof(T));

                //Database Call

            }
            catch (Exception)
            {

                throw;
            }
        }

        public T GetAll()
        {
            try
            {
                //Mock Call
                return (T)Convert.ChangeType(CreateSchoolSystem(), typeof(T));

                //Database Call

            }
            catch (Exception)
            {

                throw;
            }
        }

        private SchoolSystem CreateSchoolSystem()
        {
            //Contact database for more real information , 
            //this hardcoded data for now
            SchoolSystem students = new SchoolSystem();

           
            Student student1 = new UnderGraduateStudent();
            student1.Name = "Jeff Durbin";
            student1.Id = 20;
            student1.Addressobj.Line1 = "4816 Libert St";
            student1.Addressobj.Line2 = "Kansas City, MO";
            student1.Addressobj.Zip = "64112";
            student1.Phonenumber = "913-789-0996";

            Student student2 = new GraduateStudent();
            student2.Name = "Tyler Farabi";
            student2.Id = 245;
            student2.Addressobj.Line1 = "4816 Libert St";
            student2.Addressobj.Line2 = "Kansas City, MO";
            student2.Addressobj.Zip = "64112";
            student2.Phonenumber = "913-789-0996";

            Student student3 = new DiplomaStudent();
            student3.Name = "Aamani Kalepu";
            student3.Id = 246;
            student3.Addressobj.Line1 = "1121 North College Drive, Apt. #32";
            student3.Addressobj.Line2 = "Maryville, MO";
            student3.Addressobj.Zip = "64468";
            student3.Phonenumber = "913-789-0995";

            students.StudentList.Add(student1);
            students.StudentList.Add(student2);
            students.StudentList.Add(student3);

            return students;




        }
    }
}