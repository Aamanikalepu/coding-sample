﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Repository
{
   public interface IRepository<T>
    {
        T GetById(int Id);

        T GetAll();
    }
}
